﻿namespace ChallengeTest
{
    public class Product
    {
        public string Name { get; set; }

        private float Price { get; set; }

        public int Count { get; set; }

        public ItemType ItemType { get; set; }

        public Product(string name, float price, int count, int type)
        {
            this.Name = name;
            this.SetPrice(price);
            this.Count = count;
            this.ItemType = (ItemType)type;
        }

        public void SetPrice(float rawPrice) => this.Price = rawPrice + rawPrice * 0.16f;

        public float GetPrice() => this.Price;
    }

    public enum ItemType
    {
        Food = 1,
        Drink = 2,
    }
}
