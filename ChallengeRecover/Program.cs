﻿using System;
using System.Collections.Generic;

namespace ChallengeTest
{
    internal class Program
    {
        private static List<Product> inventory = new List<Product>();

        private static void Main(string[] args) => new Menu("Store inventory \nSelect an option: \n")
        {
            Items = {
        new MenuItem("List Products", new Action(ListProductsMenu)),
        new MenuItem("Add a Product", new Action(AddProduct)),
        new MenuItem("Modify a Products price", new Action(ModifyPriceMenu)),
        new MenuItem("Remove a Product", new Action(RemoveMenu)),
        new MenuItem("List a type", new Action(ListByTypeMenu)),
      }
        }.Run();

        public static void RemoveMenu()
        {
            Console.WriteLine("Please type the ID of the item you want to remove.\n\n");
            ListProducts(inventory);
            Console.WriteLine("\n");
            int result;
            if (!int.TryParse(Console.ReadLine(), out result) || result > inventory.Count)
                Console.WriteLine("Invalid ID, please try again.");
            else
                RemoveProduct(result - 1);
            Console.WriteLine("\n\nPress any key to exit");
            Console.ReadKey();
        }

        public static void ListProductsMenu()
        {
            ListProducts(inventory);
            Console.WriteLine("\n\nPress any key to exit");
            Console.ReadKey();
        }

        private static void ModifyPriceMenu()
        {
            Console.WriteLine("Please type the ID of the item you want to modify.\n\n");
            ListProducts(inventory);
            Console.WriteLine("\n");
            int result;
            if (!int.TryParse(Console.ReadLine(), out result) || result > inventory.Count)
                Console.WriteLine("Invalid ID, please try again.");
            else
                ModifyPrice(result - 1);
            Console.WriteLine("\n\nPress any key to exit");
            Console.ReadKey();
        }

        private static void ListByTypeMenu() => new Menu()
        {
            Header = "Please, select the type that you want to list",
            Items = {
        new MenuItem("Food", new Action(ListFoods)),
        new MenuItem("Drinks", new Action(ListDrinks))
      }
        }.Run();

        private static void ListFoods()
        {
            ListProducts(inventory.FindAll((Predicate<Product>)(p => p.ItemType == ItemType.Food)));
            Console.ReadKey();
        }

        private static void ListDrinks()
        {
            ListProducts(inventory.FindAll((Predicate<Product>)(p => p.ItemType == ItemType.Drink)));
            Console.ReadKey();
        }

        private static void RemoveProduct(int itemId)
        {
            string name = inventory[itemId].Name;
            inventory.RemoveAt(itemId);
            Console.WriteLine(name + " has been removed from the inventory.");
        }

        private static void ModifyPrice(int itemId)
        {
            Product product = inventory[itemId];
            Console.WriteLine("Product price is " + product.GetPrice().ToString() + " please, enter the new price:\n");
            product.SetPrice(ValidatePrice());
            inventory[itemId] = product;
        }

        private static void ListProducts(List<Product> products)
        {
            Console.WriteLine("\n\n");
            Console.WriteLine("ID\t\tName\t\t\tPrice\t\t\tCount\t\t\tType");
            Console.WriteLine("\n");
            for (int index = 0; index < products.Count; ++index)
            {
                string[] strArray = new string[9];
                int num = index + 1;
                strArray[0] = num.ToString();
                strArray[1] = "\t\t";
                strArray[2] = products[index].Name;
                strArray[3] = "\t\t\t";
                strArray[4] = products[index].GetPrice().ToString();
                strArray[5] = "\t\t\t";
                num = products[index].Count;
                strArray[6] = num.ToString();
                strArray[7] = "\t\t\t";
                strArray[8] = products[index].ItemType.ToString();
                Console.WriteLine(string.Concat(strArray));
            }
        }

        private static void AddProduct()
        {
            Console.WriteLine("Enter the Products name: \n");
            string name = Console.ReadLine();
            if (inventory.Find((Predicate<Product>)(p => p.Name == name)) != null)
            {
                Console.WriteLine("Product " + name + " already exists, please try a different name. \n\nPress any key to exit");
                Console.ReadKey();
            }
            else
            {
                Console.WriteLine("\nEnter the Products price: \n");
                float price = ValidatePrice();
                int result1;
                do
                {
                    Console.WriteLine("\nEnter the Products count: \n");
                    int.TryParse(Console.ReadLine(), out result1);
                    if (result1 <= 0)
                        Console.WriteLine("Count is not valid, please try again");
                }
                while (result1 <= 0);
                int result2;
                do
                {
                    Console.WriteLine("\nSelect the Products type: \n\n1.- Food\n2.- Drink");
                    int.TryParse(Console.ReadLine(), out result2);
                    if (result2 <= 0 || result2 > 2)
                        Console.WriteLine("Count is not valid, please try again");
                }
                while (result2 <= 0);
                inventory.Add(new Product(name, price, result1, result2));
            }
        }

        private static float ValidatePrice()
        {
            float result;
            do
            {
                float.TryParse(Console.ReadLine(), out result);
                if ((double)result <= 0.0)
                    Console.WriteLine("Price is not valid, please try again");
            }
            while ((double)result <= 0.0);
            return result;
        }
    }
}
