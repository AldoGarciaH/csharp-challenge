﻿using System;
using System.Collections.Generic;

namespace ChallengeTest
{
    public class Menu
    {
        public string Header { get; set; }

        public List<MenuItem> Items { get; set; }

        public Menu(string header = "")
        {
            this.Header = header;
            this.Items = new List<MenuItem>();
        }

        public void Run()
        {
            while (true)
            {
                Console.Clear();
                Console.WriteLine(this.Header);
                int index;
                for (index = 0; index < this.Items.Count; ++index)
                    Console.WriteLine(string.Format("\t{0}.- ", (object)(index + 1)) + this.Items[index].Text);
                Console.WriteLine(string.Format("\t{0}.- Exit", (object)(index + 1)));
                int result;
                if (!int.TryParse(Console.ReadLine(), out result) || result == 0 || result > this.Items.Count + 1)
                {
                    Console.WriteLine("\t ---ERROR-----\n");
                    Console.WriteLine("\t Please enter a Valid Option\n");
                    Console.ReadKey();
                }
                else if (result != this.Items.Count + 1)
                {
                    Console.Clear();
                    this.Items[result - 1].Method();
                }
                else
                    break;
            }
        }
    }

    public class MenuItem
    {
        public string Text { get; set; }

        public Action Method { get; set; }

        public MenuItem(string text, Action method)
        {
            this.Text = text;
            this.Method = method;
        }
    }
}
